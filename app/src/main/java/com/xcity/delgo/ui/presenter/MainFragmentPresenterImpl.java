package com.xcity.delgo.ui.presenter;

import android.content.Context;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.DelGOApplication;
import com.xcity.delgo.network.data.TokenPreferences;
import com.xcity.delgo.ui.presenter.domain.IMainFragmentPresenter;
import com.xcity.delgo.ui.view.domain.IMainFragmentView;

import javax.inject.Inject;

public class MainFragmentPresenterImpl implements IMainFragmentPresenter{
    private IMainFragmentView view;
    @Inject
    TokenPreferences tokenPreferences;

    @Override
    public void onLogoutPressed() {
        view.logout();
        tokenPreferences.clear();
    }

    @Override
    public void init(IMainFragmentView view, Context context) {
        this.view = view;
        getApplicationComponent(context).inject(this);
    }

    @Override
    public ApplicationComponent getApplicationComponent(Context context) {
        return DelGOApplication.get(context).getDelGOComponent();
    }
}
