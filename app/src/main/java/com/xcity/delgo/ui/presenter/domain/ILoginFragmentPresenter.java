package com.xcity.delgo.ui.presenter.domain;

import com.xcity.delgo.ui.view.domain.ILoginFragmentView;

public interface ILoginFragmentPresenter extends BaseFragmentPresenter<ILoginFragmentView>{
    void onLoginPressed(String email, String pass);
    void onForgotBtnPressed();
    void onPause();
}
