package com.xcity.delgo.ui.view.domain;

public interface ISplashFragmentView {

    void replaceToLoginFragment();

    void replaceToMainFragment();

}
