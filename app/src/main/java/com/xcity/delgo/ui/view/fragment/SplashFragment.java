package com.xcity.delgo.ui.view.fragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xcity.delgo.R;
import com.xcity.delgo.ui.di.component.MainActivityComponent;
import com.xcity.delgo.ui.presenter.SplashFragmentPresenterImpl;
import com.xcity.delgo.ui.view.BaseFragment;
import com.xcity.delgo.ui.view.domain.ISplashFragmentView;

import javax.inject.Inject;

public class SplashFragment extends BaseFragment implements ISplashFragmentView {
    @Inject
    SplashFragmentPresenterImpl presenter;

    private FragmentManager fragmentManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getComponent(MainActivityComponent.class).inject(this);
        fragmentManager = getFragmentManager();
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.init(this, getActivity());
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void replaceToLoginFragment() {
        LoginFragment loginFragment = LoginFragment.newInstance();
        replaceFragment( loginFragment, LoginFragment.TAG);
    }

    @Override
    public void replaceToMainFragment() {
        MainFragment mainFragment = MainFragment.instance();
        replaceFragment(mainFragment, MainFragment.TAG);
    }


    private void replaceFragment(Fragment fragment, String tag) {
        fragmentManager.beginTransaction()
                .replace(R.id.container_MainActivity, fragment, tag)
                .addToBackStack(null)
                .commit();
    }
}
