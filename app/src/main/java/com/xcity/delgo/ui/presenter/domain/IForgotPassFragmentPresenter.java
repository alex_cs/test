package com.xcity.delgo.ui.presenter.domain;

import com.xcity.delgo.ui.view.domain.IForgotFragmentView;

public interface IForgotPassFragmentPresenter extends BaseFragmentPresenter<IForgotFragmentView> {
    void onSendPressed(String email);

    void onPause();
}