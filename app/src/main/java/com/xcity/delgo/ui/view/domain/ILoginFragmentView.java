package com.xcity.delgo.ui.view.domain;

public interface ILoginFragmentView {
    void replaceToForgotFragment();
    void replaceToMainFragment();
    void onEmailError(String error);
    void onPasswordError(String error);
    void onLoginError(String error);
}
