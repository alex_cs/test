package com.xcity.delgo.ui.presenter;

import com.xcity.delgo.ui.presenter.domain.IMainActivityPresenter;
import com.xcity.delgo.ui.view.domain.IMainActivityView;

import javax.inject.Inject;

public class MainActivityPresenterImpl implements IMainActivityPresenter {

    private IMainActivityView view;

    @Inject
    public MainActivityPresenterImpl(IMainActivityView view) {
        this.view = view;
    }

    @Override
    public void onBackPressed() {
        view.popFragmentFromStack();
    }
}
