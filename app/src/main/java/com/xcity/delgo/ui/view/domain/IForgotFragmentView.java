package com.xcity.delgo.ui.view.domain;

public interface IForgotFragmentView {
    void onSuccess();
    void onEmailError(String error);
    void onForgotError(String error);
}
