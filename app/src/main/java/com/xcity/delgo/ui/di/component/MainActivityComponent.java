package com.xcity.delgo.ui.di.component;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.ui.di.ActivityScope;
import com.xcity.delgo.ui.di.module.MainActivityModule;
import com.xcity.delgo.ui.presenter.LoginFragmentPresenterImpl;
import com.xcity.delgo.ui.view.MainActivity;
import com.xcity.delgo.ui.view.fragment.ForgotPassFragment;
import com.xcity.delgo.ui.view.fragment.LoginFragment;
import com.xcity.delgo.ui.view.fragment.MainFragment;
import com.xcity.delgo.ui.view.fragment.SplashFragment;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class,
        modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);
    void inject(SplashFragment splashFragment);
    void inject(LoginFragment loginFragment);
    void inject(ForgotPassFragment forgotPassFragment);
    void inject(MainFragment mainFragment);
    void inject(LoginFragmentPresenterImpl loginFragmentPresenter);
}
