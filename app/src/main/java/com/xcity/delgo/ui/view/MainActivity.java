package com.xcity.delgo.ui.view;

import android.app.FragmentManager;
import android.os.Bundle;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.R;
import com.xcity.delgo.ui.di.component.DaggerMainActivityComponent;
import com.xcity.delgo.ui.di.component.IHasComponent;
import com.xcity.delgo.ui.di.component.MainActivityComponent;
import com.xcity.delgo.ui.di.module.MainActivityModule;
import com.xcity.delgo.ui.presenter.MainActivityPresenterImpl;
import com.xcity.delgo.ui.view.domain.IMainActivityView;
import com.xcity.delgo.ui.view.fragment.SplashFragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements IMainActivityView, IHasComponent<MainActivityComponent> {

    @Inject
    MainActivityPresenterImpl presenter;
    private MainActivityComponent mainActivityComponent;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getFragmentManager();
        SplashFragment splashFragment = (SplashFragment) fragmentManager.findFragmentByTag("SplashFragment");
        if (splashFragment == null) {
            splashFragment = new SplashFragment();
        }
        if (savedInstanceState == null) {
            fragmentManager.beginTransaction()
                    .add(R.id.container_MainActivity, splashFragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            presenter.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public MainActivityComponent getComponent() {
        return mainActivityComponent;
    }

    @Override
    public void popFragmentFromStack() {
        fragmentManager.popBackStack();
    }

    @Override
    protected void setupComponent(ApplicationComponent appComponent) {
        mainActivityComponent = DaggerMainActivityComponent.builder()
                .applicationComponent(appComponent)
                .mainActivityModule(new MainActivityModule(this))
                .build();
        mainActivityComponent.inject(this);
    }
}