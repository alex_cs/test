package com.xcity.delgo.ui.view.domain;

public interface IMainActivityView {
    void popFragmentFromStack();
}
