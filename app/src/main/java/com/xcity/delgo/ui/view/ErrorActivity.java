package com.xcity.delgo.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.xcity.delgo.R;
import com.xcity.delgo.ui.ErrorDialog;

public class ErrorActivity extends AppCompatActivity {

    public static Intent getErrorActivityIntent(String title, String body){
        Intent intent = new Intent();
        intent.setAction("com.xcity.delgo.ui.ErrorDialog.ErrorActivity");
        intent.putExtra(ERROR_TITLE, title);
        intent.putExtra(ERROR_BODY, body);
        return intent;
    }

    private static final String LOG_TAG = "SMSReceiver";
    public static final String ERROR_TITLE = "ERROR_TITLE";
    public static final String ERROR_BODY = "ERROR_BODY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            displayAlert(intent.getStringExtra(ERROR_TITLE),
                    intent.getStringExtra(ERROR_BODY));
        }
    }

    private void displayAlert(String title, String body) {
        ErrorDialog.showDialog(this, title, body);
    }
}
