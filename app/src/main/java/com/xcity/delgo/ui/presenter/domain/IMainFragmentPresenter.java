package com.xcity.delgo.ui.presenter.domain;

import com.xcity.delgo.ui.view.domain.IMainFragmentView;

public interface IMainFragmentPresenter extends BaseFragmentPresenter<IMainFragmentView> {
    void onLogoutPressed();
}