package com.xcity.delgo.ui.presenter.domain;

import android.content.Context;

import com.xcity.delgo.ApplicationComponent;

public interface BaseFragmentPresenter<T> {
    void init(T view, Context context);

    ApplicationComponent getApplicationComponent(Context context);

}