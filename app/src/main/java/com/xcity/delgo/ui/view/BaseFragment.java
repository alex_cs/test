package com.xcity.delgo.ui.view;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.alertdialogpro.AlertDialogPro;
import com.xcity.delgo.ui.ErrorDialog;
import com.xcity.delgo.ui.di.component.IHasComponent;

import rx.subscriptions.CompositeSubscription;

public abstract class BaseFragment extends Fragment {
    protected AlertDialogPro dialog;
    protected CompositeSubscription compositeSubscription= new CompositeSubscription();;

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }

    protected void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setStatusBarColor(color);
    }

    @SuppressWarnings("unchecked")
    protected <T> T getComponent(Class<T> componentType) {
        return componentType.cast(((IHasComponent<T>)getActivity()).getComponent());
    }

    protected void showAlertDialog(String title, String msg){
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = ErrorDialog.showDialog(getActivity(), title, msg);
    }
}
