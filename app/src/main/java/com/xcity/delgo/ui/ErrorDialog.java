package com.xcity.delgo.ui;


import android.content.Context;

import com.alertdialogpro.AlertDialogPro;

public class ErrorDialog {

    public static AlertDialogPro showDialog(Context context, String title, String msg) {
        return new AlertDialogPro.Builder(context).setTitle(title).
                setMessage(msg).
                setPositiveButton("Ok", (dialog1, which) -> {
                    dialog1.dismiss();
                }).
                show();
    }
}
