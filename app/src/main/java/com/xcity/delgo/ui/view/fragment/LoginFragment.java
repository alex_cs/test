package com.xcity.delgo.ui.view.fragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;
import com.xcity.delgo.R;
import com.xcity.delgo.GetLocationService;
import com.xcity.delgo.ui.di.component.MainActivityComponent;
import com.xcity.delgo.ui.presenter.LoginFragmentPresenterImpl;
import com.xcity.delgo.ui.view.BaseFragment;
import com.xcity.delgo.ui.view.domain.ILoginFragmentView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginFragment extends BaseFragment implements ILoginFragmentView {
    public static String TAG = LoginFragment.class.getSimpleName();
    @Inject
    LoginFragmentPresenterImpl presenter;

    @Bind(R.id.email_EditText_LoginFragment)
    EditText etEmail;
    @Bind(R.id.password_EditText_LoginFragment)
    EditText etPassword;
    @Bind(R.id.login_Button_LoginFragment)
    Button btnLogin;
    @Bind(R.id.forgot_Button_LoginFragment)
    TextView tvForgot;

    private FragmentManager fragmentManager;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getComponent(MainActivityComponent.class).inject(this);
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        bindUI(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.init(this, getActivity());
        clearBackStack();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    private void bindUI(View root) {
        ButterKnife.bind(this, root);
        compositeSubscription.add(RxView.clicks(btnLogin)
                .subscribe(aVoid -> {
                    presenter.onLoginPressed(etEmail.getText().toString(),
                            etPassword.getText().toString());
                }));
        compositeSubscription.add(RxView.clicks(tvForgot)
                .subscribe(aVoid -> {
                    presenter.onForgotBtnPressed();
                }));
        fragmentManager = getFragmentManager();
        setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.color_main_bg_start));
    }

    private void clearBackStack() {
        int count = fragmentManager.getBackStackEntryCount();
        if (count > 0) {
            FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void replaceToForgotFragment() {
        ForgotPassFragment forgotPassFragment = ForgotPassFragment.newInstance();
        replaceFragment(forgotPassFragment, ForgotPassFragment.TAG);
    }

    @Override
    public void replaceToMainFragment() {
        MainFragment mainFragment = MainFragment.instance();
        replaceFragment(mainFragment, MainFragment.TAG);
        startLocationService();
    }

    private void startLocationService() {
        getActivity().startService(new Intent(getActivity(), GetLocationService.class));
    }

    private void replaceFragment(Fragment fragment, String tag) {
        fragmentManager.beginTransaction()
                .replace(R.id.container_MainActivity, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onEmailError(String error) {
        showAlertDialog("Email error", error);
    }

    @Override
    public void onPasswordError(String error) {
        showAlertDialog("Password error", error);
    }

    @Override
    public void onLoginError(String error) {
        showAlertDialog("Login error", error);
    }
}
