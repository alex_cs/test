package com.xcity.delgo.ui.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.DelGOApplication;
import com.xcity.delgo.network.data.ApiInterface;
import com.xcity.delgo.network.data.TokenPreferences;
import com.xcity.delgo.network.domain.ResponseToken;
import com.xcity.delgo.ui.presenter.domain.ILoginFragmentPresenter;
import com.xcity.delgo.ui.view.domain.ILoginFragmentView;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginFragmentPresenterImpl implements ILoginFragmentPresenter {
    @Inject
    ApiInterface apiInterface;
    @Inject
    TokenPreferences tokenPreferences;
    private ILoginFragmentView view;
    Subscription loginSubscription;

    @Override
    public void onLoginPressed(String email, String pass) {
        if (validateLoginCredentials(email, pass)) {
            Observable<ResponseToken> loginObserver = apiInterface.authenticate(email, pass);
            loginSubscription = loginObserver
                    .doOnNext(responseToken -> tokenPreferences.token().set(responseToken.getToken()))
                    .flatMap(authToken -> apiInterface.refreshtoken(authToken.getToken()))
                    .doOnNext(token -> tokenPreferences.refreshToken().set(token.getToken()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseToken -> {
                                view.replaceToMainFragment();
                            },
                            throwable -> {
                                view.onLoginError(throwable.getMessage());
                            });
        }
    }

    private boolean validateLoginCredentials(String email, String pass) {
        boolean valid = true;
        if (TextUtils.isEmpty(email)) {
            view.onEmailError("Please enter email");
            valid = false;
        } else if (!isValidEmail(email)) {
            view.onEmailError("Enter valid email");
            valid = false;
        }else if (TextUtils.isEmpty(pass)) {
            view.onPasswordError("Please enter password");
            valid = false;
        }
        return valid;
    }

    public boolean isValidEmail(String target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void onForgotBtnPressed() {
        view.replaceToForgotFragment();
    }

    @Override
    public void onPause() {
        if (loginSubscription != null){
            loginSubscription.unsubscribe();
        }
    }


    @Override
    public void init(ILoginFragmentView view, Context context) {
        this.view = view;
        getApplicationComponent(context).inject(this);
    }

    @Override
    public ApplicationComponent getApplicationComponent(Context context) {
        return DelGOApplication.get(context).getDelGOComponent();
    }

}
