package com.xcity.delgo.ui.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.xcity.delgo.GetLocationService;
import com.xcity.delgo.R;
import com.xcity.delgo.ui.di.component.MainActivityComponent;
import com.xcity.delgo.ui.presenter.MainActivityPresenterImpl;
import com.xcity.delgo.ui.presenter.MainFragmentPresenterImpl;
import com.xcity.delgo.ui.view.BaseFragment;
import com.xcity.delgo.ui.view.domain.IMainFragmentView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;

public class MainFragment extends BaseFragment implements IMainFragmentView {
    public static final String TAG = MainFragment.class.getSimpleName();
    @Inject
    MainFragmentPresenterImpl presenter;
    @Inject
    MainActivityPresenterImpl mainActivityPresenter;

    @Bind(R.id.status_TextView_MainFragment)
    TextView tvStatus;
    @Bind(R.id.logout_Button_MainFragment)
    Button btnLogout;

    public static MainFragment instance(){
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getComponent(MainActivityComponent.class).inject(this);
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        bindUI(root);
        return root;
    }
    @Override
    public void onResume() {
        super.onResume();
        presenter.init(this, getActivity());
    }

    private void bindUI(View root){
        ButterKnife.bind(this, root);
        compositeSubscription.add(RxView
                .clicks(btnLogout)
                .subscribe(aVoid -> {presenter.onLogoutPressed();}));
    }

    @Override
    public void setStatus(String status) {
        tvStatus.setText(status);
    }

    @Override
    public void logout() {
        mainActivityPresenter.onBackPressed();
        getActivity().stopService(new Intent(getActivity(), GetLocationService.class));
    }
}
