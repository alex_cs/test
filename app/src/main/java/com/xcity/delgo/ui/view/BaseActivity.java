package com.xcity.delgo.ui.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.DelGOApplication;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(DelGOApplication.get(this).getDelGOComponent());
    }

    protected abstract void setupComponent(ApplicationComponent appComponent);
}
