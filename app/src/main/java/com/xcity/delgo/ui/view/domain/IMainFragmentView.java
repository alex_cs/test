package com.xcity.delgo.ui.view.domain;

public interface IMainFragmentView {
    void setStatus(String status);
    void logout();
}
