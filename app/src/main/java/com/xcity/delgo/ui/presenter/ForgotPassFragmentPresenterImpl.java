package com.xcity.delgo.ui.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.DelGOApplication;
import com.xcity.delgo.network.data.ApiInterface;
import com.xcity.delgo.ui.presenter.domain.IForgotPassFragmentPresenter;
import com.xcity.delgo.ui.view.domain.IForgotFragmentView;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ForgotPassFragmentPresenterImpl implements IForgotPassFragmentPresenter {
    @Inject
    ApiInterface apiInterface;
    private IForgotFragmentView view;
    Subscription forgotSubscription;


    @Override
    public void onSendPressed(String email) {
        if (validEmail(email)) {
            Observable<Void> loginObserver = apiInterface.recoverpass(email);
            forgotSubscription = loginObserver
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aVoid -> {
                                view.onSuccess();
                            },
                            throwable -> {
                                view.onForgotError(throwable.getMessage());
                            });
        }
    }

    private boolean validEmail(String email) {
        boolean valid = true;
        if (TextUtils.isEmpty(email)) {
            view.onEmailError("Please enter email");
            valid = false;
        } else if (!isValidEmail(email)) {
            view.onEmailError("Enter valid email");
            valid = false;
        }
        return valid;
    }

    private boolean isValidEmail(String target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void onPause() {
        if (forgotSubscription != null) {
            forgotSubscription.unsubscribe();
        }
    }

    @Override
    public void init(IForgotFragmentView view, Context context) {
        this.view = view;
        getApplicationComponent(context).inject(this);
    }

    @Override
    public ApplicationComponent getApplicationComponent(Context context) {
        return DelGOApplication.get(context).getDelGOComponent();
    }
}
