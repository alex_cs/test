package com.xcity.delgo.ui.di.component;

public interface IHasComponent <T> {
    T getComponent();
}
