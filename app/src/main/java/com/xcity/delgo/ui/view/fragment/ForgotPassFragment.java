package com.xcity.delgo.ui.view.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.jakewharton.rxbinding.view.RxView;
import com.rey.material.widget.EditText;
import com.xcity.delgo.R;
import com.xcity.delgo.ui.di.component.MainActivityComponent;
import com.xcity.delgo.ui.presenter.ForgotPassFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.MainActivityPresenterImpl;
import com.xcity.delgo.ui.view.BaseFragment;
import com.xcity.delgo.ui.view.domain.IForgotFragmentView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForgotPassFragment extends BaseFragment implements IForgotFragmentView {
    public static String TAG = BaseFragment.class.getSimpleName();
    @Inject
    ForgotPassFragmentPresenterImpl presenter;
    @Inject
    MainActivityPresenterImpl mainActivityPresenter;

    @Bind(R.id.email_EditText_ForgotFragment)
    EditText etEmail;
    @Bind(R.id.send_Button_ForgotFragment)
    Button btnSend;

    public static ForgotPassFragment newInstance() {
        Bundle args = new Bundle();
        ForgotPassFragment fragment = new ForgotPassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getComponent(MainActivityComponent.class).inject(this);
        View root = inflater.inflate(R.layout.fragment_forgot_pass, container, false);
        bindUI(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.init(this, getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    private void bindUI(View root) {
        ButterKnife.bind(this, root);
        compositeSubscription.add(RxView.clicks(btnSend)
                .subscribe(aVoid -> {
                    presenter.onSendPressed(etEmail.getText().toString());
                }));
    }

    @Override
    public void onSuccess() {
        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
        mainActivityPresenter.onBackPressed();
    }

    @Override
    public void onEmailError(String error) {
        showAlertDialog("Email error", error);
    }

    @Override
    public void onForgotError(String error) {
        showAlertDialog("Send error", error);
    }
}
