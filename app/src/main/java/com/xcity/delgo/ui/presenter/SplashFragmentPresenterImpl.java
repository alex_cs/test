package com.xcity.delgo.ui.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.xcity.delgo.ApplicationComponent;
import com.xcity.delgo.DelGOApplication;
import com.xcity.delgo.network.data.TokenPreferences;
import com.xcity.delgo.ui.presenter.domain.ISplashFragmentPresenter;
import com.xcity.delgo.ui.view.domain.ISplashFragmentView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;

public class SplashFragmentPresenterImpl implements ISplashFragmentPresenter {
    @Inject
    TokenPreferences tokenPreferences;
    private ISplashFragmentView view;
    private Subscription timerSubscription;
    private Observable<Long> longObservable;


    @Inject
    public SplashFragmentPresenterImpl() {
    }

    @Override
    public void onResume() {
        timerSubscription = longObservable
                .subscribe(aLong -> {
                    if (TextUtils.isEmpty(tokenPreferences.refreshToken().get())){
                        view.replaceToLoginFragment();
                    }
                    else{
                        view.replaceToMainFragment();
                    }
                });
    }

    @Override
    public void onPause() {
        timerSubscription.unsubscribe();
    }

    @Override
    public void init(ISplashFragmentView view, Context context) {
        this.view = view;
        longObservable = Observable.timer(2, TimeUnit.SECONDS);
        getApplicationComponent(context).inject(this);
    }

    @Override
    public ApplicationComponent getApplicationComponent(Context context) {
        return DelGOApplication.get(context).getDelGOComponent();
    }
}
