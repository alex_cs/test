package com.xcity.delgo.ui.di.module;

import com.xcity.delgo.ui.presenter.ForgotPassFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.LoginFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.MainActivityPresenterImpl;
import com.xcity.delgo.ui.presenter.MainFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.SplashFragmentPresenterImpl;
import com.xcity.delgo.ui.view.domain.IMainActivityView;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {
    private IMainActivityView view;

    public MainActivityModule(IMainActivityView view) {
        this.view = view;
    }

    @Provides
    public IMainActivityView provideView() {
        return view;
    }

    @Provides
    public MainActivityPresenterImpl provideMainActivityPresenterImpl(IMainActivityView view) {
        return new MainActivityPresenterImpl(view);
    }

    @Provides
    public SplashFragmentPresenterImpl provideSplashFragmentPresenterImpl() {
        return new SplashFragmentPresenterImpl();
    }

    @Provides
    public LoginFragmentPresenterImpl provideLoginFragmentPresenterImpl() {
        return new LoginFragmentPresenterImpl();
    }

    @Provides
    public ForgotPassFragmentPresenterImpl provideForgotPassFragmentPresenterImpl() {
        return new ForgotPassFragmentPresenterImpl();
    }

    @Provides
    public MainFragmentPresenterImpl provideMainFragmentPresenterImpl() {
        return new MainFragmentPresenterImpl();
    }
}

