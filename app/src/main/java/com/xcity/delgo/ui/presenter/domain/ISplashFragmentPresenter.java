package com.xcity.delgo.ui.presenter.domain;

import com.xcity.delgo.ui.view.domain.ISplashFragmentView;

public interface ISplashFragmentPresenter extends BaseFragmentPresenter<ISplashFragmentView> {
    void onResume();
    void onPause();
}
