package com.xcity.delgo.ui.presenter.domain;

public interface IMainActivityPresenter {
    void onBackPressed();
}