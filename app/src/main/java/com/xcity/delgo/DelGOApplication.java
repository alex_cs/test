package com.xcity.delgo;

import android.app.Application;
import android.content.Context;

import com.xcity.delgo.network.data.module.ApiModule;
import com.xcity.delgo.network.data.module.AppModule;
import com.xcity.delgo.network.data.module.NetModule;
import com.xcity.delgo.location.LocationManagerModule;
import com.xcity.delgo.strorage.data.module.RepositoryModule;

public class DelGOApplication extends Application {
    private ApplicationComponent delGOComponent;

    public static DelGOApplication get(Context context) {
        return (DelGOApplication) context.getApplicationContext();
    }

    public ApplicationComponent getDelGOComponent() {
        return delGOComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildComponents();
    }

    private void buildComponents() {
        delGOComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("http://188.166.52.121/delgo/public/api/"))
                .apiModule(new ApiModule())
                .locationManagerModule(new LocationManagerModule())
                .repositoryModule(new RepositoryModule())
                .build();
    }
}
