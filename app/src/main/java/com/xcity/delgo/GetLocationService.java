package com.xcity.delgo;


import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.xcity.delgo.location.GpsLocationController;

import javax.inject.Inject;

public class GetLocationService extends Service {
    @Inject
    GpsLocationController gpsLocationController;

    GPSChangeReceiver gpsChangeReceiver = new GPSChangeReceiver();
    NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DelGOApplication.get(getApplication()).getDelGOComponent().inject(this);
        registerListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterListener();
    }

    private void registerListener() {
        gpsLocationController.registerListener();
        registerReceiver(gpsChangeReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterListener() {
        gpsLocationController.unregisterListener();
        unregisterReceiver(gpsChangeReceiver);
        unregisterReceiver(networkChangeReceiver);
    }
}
