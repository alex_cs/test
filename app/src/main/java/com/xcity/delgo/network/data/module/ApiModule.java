package com.xcity.delgo.network.data.module;

import com.xcity.delgo.network.data.ApiInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;


@Module
public class ApiModule {

    @Provides
    @Singleton
    public ApiInterface provideInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }
}
