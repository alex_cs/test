package com.xcity.delgo.network.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.xmartlabs.rxsimplenosql.Entity;

import java.util.Date;

public class LocationCoordinates implements Entity {

    @SerializedName("long")
    @Expose
    private Double longitude;

    @SerializedName("lat")
    @Expose
    private Double latitude;

    @SerializedName("date")
    @Expose
    private Date date;

    @Expose(serialize = false, deserialize = false)
    private String id;

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public LocationCoordinates(Double longitude, Double latitude, Long date){
        this.latitude = latitude;
        this.longitude = longitude;
        //2015-10-02 16:55:46
        this.date = new Date(date);
    }


    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return null;
    }
}
