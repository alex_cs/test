package com.xcity.delgo.network.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;

import javax.inject.Inject;

public class TokenPreferences {
    public static final String PREFERENCE_FOLDER_PREFIX = "delgo_preferences";

    public static final String TOKEN = "TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    private final RxSharedPreferences rxSharedPreferences;

    @Inject
    TokenPreferences(Application context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCE_FOLDER_PREFIX, Context.MODE_PRIVATE);
        this.rxSharedPreferences = RxSharedPreferences.create(sharedPreferences);
    }

    public Preference<String> token() {
        return rxSharedPreferences.getString(TOKEN, "");
    }

    public Preference<String> refreshToken() {
        return rxSharedPreferences.getString(REFRESH_TOKEN, "");
    }

    public void clear(){
        token().delete();
        refreshToken().delete();
    }
}
