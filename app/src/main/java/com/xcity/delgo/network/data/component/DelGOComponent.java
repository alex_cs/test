package com.xcity.delgo.network.data.component;

import com.xcity.delgo.network.data.ApiInterface;
import com.xcity.delgo.ui.presenter.ForgotPassFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.LoginFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.MainFragmentPresenterImpl;
import com.xcity.delgo.ui.presenter.SplashFragmentPresenterImpl;

public interface DelGOComponent {
    ApiInterface getApiInterface();
    void inject(LoginFragmentPresenterImpl loginFragmentPresenter);
    void inject(ForgotPassFragmentPresenterImpl forgotPassFragmentPresenter);
    void inject(SplashFragmentPresenterImpl splashFragmentPresenter);
    void inject(MainFragmentPresenterImpl mainFragmentPresenter);
}
