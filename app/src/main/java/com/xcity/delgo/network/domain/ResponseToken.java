package com.xcity.delgo.network.domain;

import com.google.gson.annotations.SerializedName;

public class ResponseToken {
    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
