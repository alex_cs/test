package com.xcity.delgo.network.data.component;

import android.app.Application;

import com.google.gson.Gson;

import retrofit2.Retrofit;


public interface NetComponent {
    Retrofit getRetrofit();
    Application getApplication();
    Gson getGson();
}
