package com.xcity.delgo.network.data;

import com.xcity.delgo.network.domain.LocationCoordinates;
import com.xcity.delgo.network.domain.ResponseToken;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;


public interface ApiInterface {

    @FormUrlEncoded
    @POST("authenticate")
    Observable<ResponseToken> authenticate(@Field("email") String email,
                                    @Field("password") String password);

    @GET("refreshtoken")
    Observable<ResponseToken> refreshtoken(@Query("token") String token);


    @FormUrlEncoded
    @POST("recoverpass")
    Observable<Void> recoverpass(@Field("email") String email);

    @FormUrlEncoded
    @POST("setgeo")
    Observable<Void> setgeo(@Field("geo") String geo);

    @FormUrlEncoded
    @POST("setgeoarray")
    Observable<Void> setgeoarray(@Field("geo") List<LocationCoordinates> locationCoordinates);

}
