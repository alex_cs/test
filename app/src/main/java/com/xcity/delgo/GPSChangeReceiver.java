package com.xcity.delgo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import com.xcity.delgo.ui.ErrorDialog;

import javax.inject.Inject;


public class GPSChangeReceiver extends BroadcastReceiver {
    @Inject
    LocationManager locationManager;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        DelGOApplication.get(context).getDelGOComponent().inject(this);
        locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
           {
               ErrorDialog.showDialog(context, "Network Connection Lost",
                       "App will store information about GPS and send it while Network will be available.");
        }
    }
}