package com.xcity.delgo.strorage.data;

import com.xcity.delgo.network.domain.LocationCoordinates;
import com.xcity.delgo.strorage.domain.ILocationRepository;
import com.xmartlabs.rxsimplenosql.Bucket;

import java.util.List;

import rx.Completable;
import rx.Observable;
import rx.functions.Func1;

public class DBLocationRepository implements ILocationRepository<LocationCoordinates> {

    private final Bucket<LocationCoordinates> bucket;

    public DBLocationRepository(Bucket<LocationCoordinates> bucket) {
        this.bucket = bucket;
    }

    @Override
    public void add(LocationCoordinates item) {
        bucket.newQuery()
                .save(item)
                .subscribe();
    }

    @Override
    public void addAll(List<LocationCoordinates> items) {
        bucket.newQuery()
                .save(items)
                .subscribe();
    }

    @Override
    public Observable<LocationCoordinates> get(Func1<LocationCoordinates, Boolean> predicate) {
        return bucket.newQuery()
                .filter(predicate)
                .retrieve();
    }

    @Override
    public Observable<List<LocationCoordinates>> getAll() {
        return Observable.empty();
    }

    @Override
    public void remove(Func1<LocationCoordinates, Boolean> predicate) {
        bucket.newQuery()
                .filter(predicate)
                .delete()
                .subscribe();
    }

    @Override
    public void removeAll() {
        bucket.newQuery()
                .delete()
                .subscribe();
    }
}
