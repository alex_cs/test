package com.xcity.delgo.strorage.data.module;

import android.app.Application;

import com.xcity.delgo.network.domain.LocationCoordinates;
import com.xcity.delgo.strorage.data.DBLocationRepository;
import com.xmartlabs.rxsimplenosql.Bucket;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    Bucket<LocationCoordinates> provideDB(Application application){
        return  new Bucket<>(application, LocationCoordinates.class, "bucketId");
    }

    @Provides
    @Singleton
    DBLocationRepository provideRepository(Bucket<LocationCoordinates> bucket){
        return new DBLocationRepository(bucket);
    }

}
