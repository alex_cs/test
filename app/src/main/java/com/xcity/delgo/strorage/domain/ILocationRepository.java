package com.xcity.delgo.strorage.domain;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

public interface ILocationRepository<T> {

    void add(T item);

    void addAll(List<T> items);

    Observable<T> get(Func1<T, Boolean> predicate);

    Observable<List<T>> getAll();

    void remove(Func1<T, Boolean>  predicate);

    void removeAll();
}
