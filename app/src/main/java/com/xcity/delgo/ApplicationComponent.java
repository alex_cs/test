package com.xcity.delgo;

import com.xcity.delgo.network.data.component.DelGOComponent;
import com.xcity.delgo.network.data.component.NetComponent;
import com.xcity.delgo.network.data.module.ApiModule;
import com.xcity.delgo.network.data.module.AppModule;
import com.xcity.delgo.network.data.module.NetModule;
import com.xcity.delgo.location.LocationComponent;
import com.xcity.delgo.location.LocationManagerModule;
import com.xcity.delgo.strorage.data.module.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetModule.class,
        ApiModule.class,
        LocationManagerModule.class,
        RepositoryModule.class
})
public interface ApplicationComponent extends
        NetComponent,
        DelGOComponent,
        LocationComponent {
}

