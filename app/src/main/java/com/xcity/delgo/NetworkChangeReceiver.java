package com.xcity.delgo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.xcity.delgo.location.GpsLocationController;
import com.xcity.delgo.ui.view.ErrorActivity;

import javax.inject.Inject;

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Inject
    GpsLocationController gpsLocationController;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        DelGOApplication.get(context).getDelGOComponent().inject(this);
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
        if (isConnected)
            gpsLocationController.sendAllFromDB();
        else{
            Intent error = ErrorActivity.getErrorActivityIntent("GPS Connection Lost", "App will not be able to send correct data about current localisation");
            error.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(error);
        }
    }
}