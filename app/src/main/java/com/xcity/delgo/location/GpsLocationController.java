package com.xcity.delgo.location;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.xcity.delgo.network.data.ApiInterface;
import com.xcity.delgo.network.data.TokenPreferences;
import com.xcity.delgo.network.domain.LocationCoordinates;
import com.xcity.delgo.strorage.data.DBLocationRepository;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

public class GpsLocationController {
    //    private final long UPDATE_FREQUENCY_TIME = 15 * 60 * 1000;
    private final long UPDATE_TIME = TimeUnit.MINUTES.toMillis(2);
    private final long REFRESH_TOKEN_TIME = 1;
    private final int SEND_RETRY_COUNT = 2;
    @Inject
    LocationManager locationManager;
    @Inject
    DBLocationRepository repository;
    @Inject
    ApiInterface apiInterface;
    @Inject
    TokenPreferences tokenPreferences;
    @Inject
    Gson gson;
    private final PublishSubject<Location> locationPublishSubject;
    private CompositeSubscription compositeSubscription;
    private Subscription refreshTokenSubscription;

    @Inject
    public GpsLocationController() {
        locationPublishSubject = PublishSubject.create();
    }

    // Define a listener that responds to location updates
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            Log.d("GPS", "location " + location.toString());
            locationPublishSubject.onNext(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    public void registerListener() {
        compositeSubscription = new CompositeSubscription();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_TIME, 0, locationListener);
        compositeSubscription.add(locationPublishSubject
                .debounce(15, TimeUnit.MINUTES)
                .subscribe(this::sendPosition));
        compositeSubscription.add(
                Observable
                        .interval(REFRESH_TOKEN_TIME, TimeUnit.MINUTES)
                        .flatMap(aLong -> apiInterface.refreshtoken(tokenPreferences.token().get()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(responseToken -> tokenPreferences.refreshToken().set(responseToken.getToken()),
                                throwable -> {}));
    }

    public void unregisterListener() {
        locationManager.removeUpdates(locationListener);
        compositeSubscription.unsubscribe();
    }

    private void sendPosition(Location location) {
        long time = Calendar.getInstance().getTimeInMillis();
        final LocationCoordinates coordinates = new LocationCoordinates(
                location.getLongitude(),
                location.getLatitude(),
                time
        );
        Observable<Void> sendObservable = apiInterface.setgeo(gson.toJson(coordinates).toString());

        sendObservable
                .retry(SEND_RETRY_COUNT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                        },
                        throwable -> {
                            saveToDB(coordinates);
                        });
    }

    private void saveToDB(LocationCoordinates coordinates) {
        repository.add(coordinates);
    }

    public void sendAllFromDB() {
        repository
                .getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(locationCoordinates -> locationCoordinates != null)
                .filter(locationCoordinates -> !locationCoordinates.isEmpty())
                .flatMap(locationCoordinates ->
                        apiInterface.setgeoarray(locationCoordinates))
                .retry(SEND_RETRY_COUNT)
                .subscribe(aVoid -> {
                            repository.removeAll();
                        },
                        throwable -> {
                        });
    }
}
