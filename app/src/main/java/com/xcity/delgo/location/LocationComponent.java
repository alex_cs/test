package com.xcity.delgo.location;

import com.xcity.delgo.GPSChangeReceiver;
import com.xcity.delgo.GetLocationService;
import com.xcity.delgo.NetworkChangeReceiver;

public interface LocationComponent {
    void inject(GetLocationService getLocationService);
    void inject(NetworkChangeReceiver networkChangeReceiver);
    void inject(GPSChangeReceiver gpsChangeReceiver);
}
